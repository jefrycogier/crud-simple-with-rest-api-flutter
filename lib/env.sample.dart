/*class Env {
  static String URL_PREFIX = "http://10.0.2.2/api/webusagi/flutter";
}*/

const BASE_URL = 'http://192.168.43.117/api/webusagi/flutter';
//const BASE_URL = 'http://10.0.2.2/api/webusagi/flutter';
//const BASE_URL = 'http://cogier.000webhostapp.com';
/*'flutter/get-all-genre' => 'flutter/get-all-genre',
'flutter/edit-genre' => 'flutter/edit-genre',
'flutter/delete-genre' => 'flutter/delete-genre',
'flutter/insert-genre' => 'flutter/insert-genre',*/

const GETALLS = BASE_URL + '/get-all-genre';
const ADDS = BASE_URL + '/insert-genre';
const UPDATES = BASE_URL + '/edit-genre';
const DELETES = BASE_URL + '/delete-genre';